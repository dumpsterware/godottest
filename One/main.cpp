#include <stdio.h>
#include <iostream>
#include <vector>


int main(int argc, char **argv)
{
	
	std::vector<char> Vowels  {'a', 'e', 'i', 'o', 'u'};
	
	std::string Taco{"tacos are good"};
	
	std::cout << "Today is a good day! I will let the suffering define who I am, and let it make my happiness shine!" << std::endl;
	
	std::cout << "if tacos are good, type \"tacos are good\":" << std::endl;
	
	std::getline(std::cin, Taco);
	
	if (Taco == "tacos are good") {
		
		std::cout << "tacos are good" << std::endl;
		
	} else std::cout << "tacos aren't good" << std::endl;
	
	std::cout << Vowels.at(0) << std::endl;
	
	return 0;
	
}
