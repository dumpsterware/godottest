.PHONY: clean All

All:
	@echo "----------Building project:[ One - Debug ]----------"
	@cd "One" && "$(MAKE)" -f  "One.mk"
clean:
	@echo "----------Cleaning project:[ One - Debug ]----------"
	@cd "One" && "$(MAKE)" -f  "One.mk" clean
